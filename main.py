from bs4 import BeautifulSoup
import yadisk
import asyncio
import calendar
import time
import aiohttp
import aiofiles
import requests
import time
from tqdm import tqdm
import os

token = "AgAAAAAhZ4KsAAW41rL6SvxHGUgchXDR13K7A9c"
LOGINS = ["flexabird13", "fryua"]
URL = "https://storiesig.com/stories/"
downloaded_images = [] 
y=yadisk.YaDisk(token=token)

async def download(received_images):
    received_images_length = len(received_images)
    print("total to download:", received_images_length)
    pbar = tqdm(total=received_images_length)
    filename=len(downloaded_images)
    counter = 0
    for item in received_images:
        #write to yadisk
        file_path=""
        ts = calendar.timegm(time.gmtime())
        if item['type'] == "image":
            file_path = "/plunder/fryua/"+str(ts)+".jpg"
        if item['type'] == "video":
            file_path = "/plunder/fryua/"+str(ts)+".mp4"
        y.upload_url(item['contents'], file_path)
        time.sleep(3)
        # write to local storage
        #filename+=1
        #counter+=1
        #async with aiohttp.ClientSession() as session:
            #async with session.get(item['contents']) as resp:
                #if resp.status == 200:
                    #file_path = ""
                    #if item['type'] == "image":
                     #   file_path = str(filename)+".jpg"
                    #if item['type'] == "video":
                        #file_path = str(filename)+".mp4"
                    #async with aiofiles.open(file_path, 'wb') as fd:
                     #   while True:
                      #      chunk = await resp.content.read(1024)
                       #     if not chunk:
                        #        break
                         #   await fd.write(chunk)
        pbar.update(1)
        downloaded_images.append(item)
    pbar.close()
        


async def get():
    while True:
        for login in LOGINS:
            user_url = URL+login
            print("current login ", login)
            print("url to download ", user_url)
            page = requests.get(user_url)
            soup = BeautifulSoup(page.text, 'html.parser')
            received_images = []
            for img in soup.findAll('img'): 
                received_images.append(
                    {
                        'type': "image",
                        'contents': img.get('src') 
                    }
                )
            for video in soup.findAll('video'):
                received_images.append(
                    {
                        'type': "video",
                        'contents': video.get('src') 
                    }
                )
            # received_images.append(video.get('src'))
            items_to_download = [item for item in received_images if not item in downloaded_images]
            received_images_length = len(received_images)
            print("total received:", received_images_length)
            await download(items_to_download)
            print("\n")
            time.sleep(15)


loop = asyncio.get_event_loop()
loop.run_until_complete(get())
loop.close()
